/*
    This is a program for assigning atom types using Open Babel.
    It is distributed along with STaGE, but can be distributed
    on its own.

    Written by Magnus Lundborg
    Copyright (c) 2013-2014, The GROMACS development team.
    Check out http://www.gromacs.org for more information.

    This code is released under GNU GPL v2.0 to match the
    Open Babel license.
*/

#include <iostream>

#include <openbabel/obconversion.h>
#include <openbabel/mol.h>
#include <openbabel/parsmart.h>
// #include <openbabel/typer.h>
using namespace OpenBabel;

int main(int argc, char **argv)
{
    std::string mol2Filename, smartsPattern;
    OBConversion obConversion;
    OBMol mol;
    OBSmartsPattern smarts;
//     OBAromaticTyper typer;

    obConversion.SetInFormat("mol2");

    if(argc < 3)
    {
        std::cout << "This program takes a SMARTS pattern string (matching an atom type definition)" << std::endl;
        std::cout << "and returns a list of atom numbers matching the definition." << std::endl << std::endl;
        std::cout << "Usage:" << std::endl << argv[0] << " <SMARTS pattern> <mol2 file>" << std::endl;
        std::cout << "Example:" << std::endl << argv[0] << " '[$([OH][#6])]' ethanol.mol2" << std::endl;
        return 1;
    }

    smartsPattern = argv[1];
    mol2Filename = argv[2];

    obConversion.ReadFile(&mol, mol2Filename);
//     typer.AssignAromaticFlags(mol);

//     std::cout << "NumAtoms in mol: " << mol.NumAtoms() << " NumBonds in mol: " << mol.NumBonds() << std::endl;

    smarts.Init(smartsPattern);
    if(!smarts.IsValid())
    {
        std::cout << "Invalid SMARTS pattern: " << smartsPattern << std::endl;
    }

//     std::cout << "NumAtoms in SMARTS: " << smarts.NumAtoms() << " NumBonds in SMARTS: " << smarts.NumBonds() << std::endl;

//     std::cout << smarts.HasMatch(mol) << std::endl;

    if(smarts.Match(mol))
    {
//         std::cout << "NumMatches:" << smarts.NumMatches() << std::endl;
        smarts.WriteMapList(std::cout);
    }

    return 0; // exit with success
}

