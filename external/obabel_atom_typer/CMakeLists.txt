# set the module path
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY bin)

include_directories(${OPENBABEL3_INCLUDE_DIR})

add_executable(obabel_atom_typer obabel_atom_typer.cpp)
target_link_libraries(obabel_atom_typer ${OPENBABEL3_LIBRARIES})
